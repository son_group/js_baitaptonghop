var arrayinput = new Array(0);
console.log("array length", arrayinput.length);

function inputArray() {
  var number = document.getElementById("txt-number").value * 1;
  arrayinput.push(number);
  var contentHTML = "";
  //   console.log("array length", arrayinput.length);
  for (var i = 0; i < arrayinput.length; i++) {
    var numberArr = arrayinput[i];
    contentHTML += `<span class="mr-5">${numberArr}</span>`;
    // console.log({contentHTML});
  }
  document.getElementById("array_input").innerHTML = contentHTML;
}

function refreshResult(element) {
  // console.log("say yes");
  // console.log({element});
  document.getElementById(element).innerHTML = "";
}

function refreshResult1() {
  var elementName = document.querySelector("#result1").id;
  refreshResult(elementName);
}
// exercise 1
function printTable() {
  var contentHTML = "";
  for (var i = 1; i <= 100; i = i + 10) {
    // console.log(i);
    var rowdata = `<tr>`;
    for (var j = 0; j < 10; j++) {
      // console.log(j);
      var value = i + j;
      rowdata = rowdata + `<td>${value}</td>`;
    }
    rowdata = rowdata + `</tr>`;
    contentHTML = contentHTML + rowdata;

    console.log(rowdata);
  }

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// exercise 2
function isPrime(element) {
  if (element == 0 || element == 1) {
    return false;
  } else if (element == 2) {
    return true;
  } else {
    for (var i = 2; i < element; i++) {
      if (element % i == 0) {
        return false;
      }
    }
    return true;
  }
}

function findPrimeInArray() {
  var prime = arrayinput.filter((element) => isPrime(element));
  console.log({ prime });
  if (prime == null) {
    document.getElementById("result2").innerHTML = `Mảng không có số nguyên tố`;
  } else {
    console.log(prime);
    document.getElementById("result2").innerHTML = prime;
  }
}

// exercise 3
function findSumOfN() {
  var n = document.getElementById("txt-number_ex3").value * 1;
  var sum = n;
  for (var i = 2; i < n; i++) {
    sum += i;
  }
  sum += 2 * n;
  document.getElementById("result3").innerHTML = `Tổng cần tính là ${sum}`;
}

// exerciser 4

function findDivisor() {
  var n = document.getElementById("txt-number_ex4").value * 1;
  var divisorlist = [];
  for (var i = 1; i * 2 <= n; i++) {
    if (n % i == 0) {
      divisorlist.push(i);
    }
    console.log("i ", i);
    console.log("chia ", n % i);
  }
  divisorlist.push(n);
  document.getElementById(
    "result4"
  ).innerHTML = `Ước của ${n} là ${divisorlist}`;
}

// exercise 5
function reverseNumber() {
  var number = document.getElementById("txt-number_ex5").value * 1;
  var content = "";
  while (number > 10) {
    var subnumber = number % 10;
    content += subnumber;
    number = Math.floor(number / 10);
  }
  content += number;
  document.getElementById("result5").innerText = content;
}

function calculateSum(number) {
  var sum = number;
  for (var i = 0; i < number; i++) {
    sum += i;
  }
  return sum;
}

// exercise 6
function findNumberSumLessThan100() {
  var x = 1;
  while (calculateSum(x) < 100) {
    x += 1;
  }
  console.log("sum: ", calculateSum(x - 1));
  document.getElementById("result6").innerText = x - 1;
}

//excercíe 7
function printMultiplicationTable() {
  var number = document.getElementById("txt-number_ex7").value * 1;
  var contentHTML = "";

  for (var i = 0; i <= 10; i++) {
    var value = number * i;
    var row = `<tr><td>${number}*${i} = ${value}</td></tr>`;
    contentHTML += row;
  }

  console.log("contentHTML ", contentHTML);
  document.getElementById("tbodyMultiplicationTable").innerHTML = contentHTML;
}

// exercise 8
function dividePoker() {
  var cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];
  var player = [[], [], [], []];
  var sublength = player.length;
  // console.log("sublength ",sublength);
  for (var i = 0; i < cards.length; i++) {
    var index = i % sublength;
    var number = cards[i];
    player[index].push(number);
    // console.log("index ",index);
    // console.log("card value:", number);
    // // console.log("mang con",player);
    // console.log(player[index].length);
  }
  var contentHTML = "";
  for (var index = 0; index < player.length; index++) {
    var rowcontent = "<tr>";
    var playerpoker = player[index];
    for (var subindex = 0; subindex < playerpoker.length; subindex++) {
      rowcontent += `<td>${playerpoker[subindex]}</td>`;
    }
    rowcontent += "</tr>";
    contentHTML += rowcontent;
  }
  document.getElementById("tbodypoker").innerHTML = contentHTML;
}

//exercise 9
function findChickandDog() {
  var dog_chicken = document.getElementById("txt-number-chicken_dog").value * 1;
  var food_dog_chicken =
    document.getElementById("txt-footnumber-chicken_dog").value * 1;

  if ((food_dog_chicken - 2 * dog_chicken) % 2 != 0) {
    document.getElementById(
      "result9"
    ).innerHTML = `Số con vật và số chân không hợp lý`;
  } else {
    var dog = (food_dog_chicken - 2 * dog_chicken) / 2;
    var chicken = dog_chicken - dog;
    document.getElementById(
      "result9"
    ).innerHTML = `Số gà và chó là: ${chicken} và ${dog}`;
  }
}

//exercise 10
function findAngle() {
  var hour = document.getElementById("txt-number_ex10_hour").value * 1;
  var minute = document.getElementById("txt-number_ex10_minute").value * 1;

  var angleByMinute;
  var angleByHour;
  if (minute == 60) {
    angleByMinute = 0;
  } else {
    angleByMinute = 6 * minute;
  }

  if (hour >= 12) {
    hour = hour - 12;
  }
  if (hour < 1) {
    angleByHour = 0.5 * minute;
  } else {
    angleByHour = hour * 30 + 0.5 * minute;
  }

  var angle = Math.abs(angleByHour - angleByMinute);
  document.getElementById(
    "result10"
  ).innerHTML = `Góc giữa kim giờ và phút là ${angle}`;
}
